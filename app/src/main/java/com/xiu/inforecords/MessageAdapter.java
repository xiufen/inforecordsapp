package com.xiu.inforecords;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by xiu on 2015/8/4.
 */
public class MessageAdapter extends ArrayAdapter<Message> {


    private Context context;
    private List<Message> list;

    TextView title;
    TextView info;
    TextView time;

    public MessageAdapter(Context context, List<Message> list) {
        super(context, 0, list);
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Message getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.itemactivity, null);
        }

        Message message =getItem(position);
        title = (TextView)convertView.findViewById(R.id.title);
        info = (TextView)convertView.findViewById(R.id.info);
        time = (TextView)convertView.findViewById(R.id.time);

        title.setText(message.getTitle());
        info.setText(message.getInfo());
        time.setText(DateTools.format(message.getCreatedAt()));
        return convertView;
    }
}
