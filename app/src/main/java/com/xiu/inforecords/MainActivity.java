package com.xiu.inforecords;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements RefreshAndLoadListView.OnRefreshListener{

    private static final String TAG = "MainActivity";

    private RefreshAndLoadListView listView;
    private int page = 0;
    private List<Message> listMessage;
    private MessageAdapter adapter;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate ");
        context = this;
        listView = (RefreshAndLoadListView) findViewById(R.id.listView);
        listViewListener();
    }

    public void showItem() {
        Log.d(TAG, "showItem ");
        listMessage = new ArrayList<>();
        FindCallback<Message> findCallBack = new FindCallback<Message>() {
            @Override
            public void done(List<Message> list, AVException e) {
                if (list.size() > 0) {
                    listMessage.addAll(list);
                    adapter.notifyDataSetChanged();
                    page++;
                }
            }
        };
        AVService.findMessages(page, null, findCallBack);
        adapter = new MessageAdapter(this, listMessage);
        listView.setAdapter(adapter);
        listView.setOnRefreshListener(this);
        adapter.notifyDataSetChanged();
    }

    public void listViewListener() {

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if (listView.currentState != listView.DOWN_PULL_REFRESH) {
                    return false;
                }
                Log.d(TAG, "onItemLongClick ==============  position  " + position);
                Log.d(TAG, "onItemLongClick ==============  long  " + id);
                final int _position = position;
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                alert.setIcon(R.mipmap.services)
                        .setMessage("是否要删除此项？")
                        .setPositiveButton("是", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                GetCallback<AVObject> getCallBack = new GetCallback<AVObject>() {
                                    @Override
                                    public void done(AVObject avobject, AVException e) {
                                        if (e == null && avobject != null) {
                                            avobject.deleteInBackground();
                                            listMessage.remove(_position-1);
                                            adapter.notifyDataSetChanged();
                                        }
                                    }
                                };
                                AVService.fetchById(listMessage.get(_position-1).getObjectId(), getCallBack);
                            }
                        })
                        .setNegativeButton("否", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alert.show();
                return true;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (listView.currentState != listView.DOWN_PULL_REFRESH) {
                    return;
                }
                Intent intent = new Intent(MainActivity.this, ModifyAndSaveActivity.class);
                Bundle bundle = new Bundle();
                Log.d(TAG, "onItemClick ------------ position  " + position);
                Log.d(TAG, "onItemClick ------------ long  " + id);
                bundle.putString("index", listMessage.get(position-1).getObjectId());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_search) {
            Intent intent = new Intent(MainActivity.this, SearchActivity.class);
            startActivity(intent);
        }

        if (id == R.id.action_save) {
            Intent intent = new Intent(MainActivity.this, ModifyAndSaveActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ");
        listMessage.clear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ");
        page = 0;
        showItem();
    }

    @Override
    public void onDownPullRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FindCallback<Message> findCallBack = new FindCallback<Message>() {
                    @Override
                    public void done(List<Message> list, AVException e) {
                        if (e == null) {
                            listMessage.addAll(0, list);
                        }
                        listView.hideHeaderView();
                    }
                };
                int page = 0;
                AVService.findMessages(page, listMessage.get(0).getCreatedAt(), findCallBack);
                adapter = new MessageAdapter(context, listMessage);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.d(TAG, "onRefresh ");
            }
        }, 3000);
    }

    @Override
    public void onLoadingMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                FindCallback<Message> findCallBack = new FindCallback<Message>() {
                    @Override
                    public void done(List<Message> list, AVException e) {
                        if (list.size() > 0) {
                            listMessage.addAll(list);
                            adapter.notifyDataSetChanged();
                            page++;
                        }
                        listView.hideFooterView();
                    }
                };
                AVService.findMessages(page, null, findCallBack);
                adapter = new MessageAdapter(context, listMessage);
                listView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                Log.d(TAG, "loadMore ");
            }
        }, 3000);
    }
}
