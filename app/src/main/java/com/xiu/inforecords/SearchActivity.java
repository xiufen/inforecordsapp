package com.xiu.inforecords;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.SearchView;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {

    private ListView listView;
    private SearchView searchView;
    private List<Message> listMessage;
    private Context context;
    private MessageAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        context = this;

        initActionbar();
        listView = (ListView) findViewById(R.id.search_list);
        listView.setTextFilterEnabled(true);

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                AVQuery<Message> search = AVQuery.getQuery(Message.class);
                search.whereContains("title", newText);
                search.orderByDescending("createdAt");
                search.findInBackground(new FindCallback<Message>() {
                    @Override
                    public void done(List<Message> list, AVException e) {
                        listMessage = new ArrayList<Message>();
                        listMessage.clear();
                        if (e == null) {
                            listMessage.addAll(list);
                            adapter = new MessageAdapter(context, listMessage);
                            listView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }
                });
                return true;
            }
        });
        searchView.setSubmitButtonEnabled(false);

    }

    public void initActionbar() {

        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mTitleView = mInflater.inflate(R.layout.search_action_bar, null);
        getSupportActionBar().setCustomView(mTitleView, new ActionBar.LayoutParams(LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT));
                searchView = (SearchView) mTitleView.findViewById(R.id.search_view);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
