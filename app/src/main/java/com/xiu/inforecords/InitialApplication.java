package com.xiu.inforecords;

import android.app.Application;

import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVObject;

/**
 * Created by miluo on 18/8/15.
 */
public class InitialApplication extends Application {

    private static final String TAG = "InitialApplication";

    @Override
    public void onCreate() {
        super.onCreate();
        AVObject.registerSubclass(Message.class);
        AVOSCloud.initialize(this, Config.APP_ID, Config.APP_KEY);

    }
}
