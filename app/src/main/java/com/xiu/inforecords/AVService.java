package com.xiu.inforecords;

import android.text.TextUtils;

import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.FindCallback;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;

import java.util.Date;

/**
 * Created by xiu on 2015/8/19.
 */
public class AVService {

    private static final String TAG = "AVService";

    public static void findMessages(int page, Date createdAt, FindCallback<Message> findCallback) {
        AVQuery<Message> query = AVQuery.getQuery(Message.class);
        if (createdAt != null) {
            if (page == 0) {
                query.whereGreaterThan("createdAt", createdAt);
            } else {
                query.whereLessThan("createdAt", createdAt);
            }
        }
        query.skip(page * Config.PAGE_SIZE);  //跳过page页
        query.orderByDescending("createdAt"); //以createdAt降序排列
        query.limit(Config.PAGE_SIZE);     //限制查询的大小为一页的内容，10
        query.findInBackground(findCallback);
    }

    public static void fetchById(String objectId, GetCallback<AVObject> getCallback) {
        final Message message = new Message();
        message.setObjectId(objectId);
        message.fetchInBackground(getCallback);
    }

    public static void createOrUpdate(String objectId, String title, String info, SaveCallback saveCallback) {
        final Message message = new Message();
        if (!TextUtils.isEmpty(objectId)) {
            message.setObjectId(objectId);
        }
        message.setTitle(title);
        message.setInfo(info);
        message.saveInBackground(saveCallback);
    }

}
