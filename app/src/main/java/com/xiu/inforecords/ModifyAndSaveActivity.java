package com.xiu.inforecords;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;

import java.util.Calendar;

public class ModifyAndSaveActivity extends AppCompatActivity {

    private static final String TAG = "ModifyAndSaveActivity";

     private EditText editTitle;
     private EditText editTime;
     private EditText editInfo;
     private static String objectId;
    private Message message;

    @Override
   protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_and_save);
        initView();
        Log.d(TAG, "onCreate ");
        objectId = getIntent().getStringExtra("index");
        if (!TextUtils.isEmpty(objectId)) {
            initialData(objectId);
        }

        editTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                new DatePickerDialog(ModifyAndSaveActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        editTime.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                        Calendar timeCalendar = Calendar.getInstance();
                        TimePickerDialog timeDialog = new TimePickerDialog(ModifyAndSaveActivity.this,
                                new TimePickerDialog.OnTimeSetListener() {

                                    @Override
                                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                        editTime.append(" " + hourOfDay + ":" + minute);
                                    }
                                }, timeCalendar.get(Calendar.HOUR_OF_DAY), timeCalendar.get(Calendar.MINUTE), true);
                        timeDialog.show();
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    public void initView() {
        editTitle = (EditText)findViewById(R.id.edit_title);
        editInfo = (EditText)findViewById(R.id.edit_info);
        editTime = (EditText)findViewById(R.id.edit_time);
    }

    private void initialData(String objectId) {
        GetCallback<AVObject> getCallback = new GetCallback<AVObject>() {
            @Override
            public void done(AVObject msg, AVException arg1) {
                if (msg != null) {
                    message = (Message) msg;
                    editTitle.setText(message.getTitle());
                    editInfo.setText(message.getInfo());
                    editTime.setText(DateTools.format(message.getCreatedAt()));
                }
            }
        };
        AVService.fetchById(objectId, getCallback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_modify_and_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_modify) {
            Log.d(TAG, "onOptionsItemSelected ");
            SaveCallback saveCallBack = new SaveCallback(){
                @Override
                public void done(AVException e) {
                    finish();
                }
            };
            AVService.createOrUpdate(objectId, editTitle.getText().toString(), editInfo.getText().toString(), saveCallBack);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "onStop ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume ");
    }
}
