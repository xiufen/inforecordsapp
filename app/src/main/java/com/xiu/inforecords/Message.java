package com.xiu.inforecords;

import com.avos.avoscloud.AVClassName;
import com.avos.avoscloud.AVObject;

/**
 * Created by xiu on 2015/8/3.
 */
@AVClassName("Message")
public class Message extends AVObject{
    public String getTitle() {
        return getString("title");
    }
    public void setTitle(String value) {
        put("title", value);
    }
    public String getInfo() {
        return getString("info");
    }
    public void setInfo(String value) {
        put("info", value);
    }
}
