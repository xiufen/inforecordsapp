package com.xiu.inforecords;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.text.SimpleDateFormat;

/**
 * Created by xiu on 2015/8/19.
 */
public class RefreshAndLoadListView extends ListView implements AbsListView.OnScrollListener{

    private static final String TAG = "RefreshAndLoadView";

    private OnRefreshListener onRefershListener;
    private int firstVisiblePosition;
    private int dowmY;
    public static final int DOWN_PULL_REFRESH = 0; // 下拉刷新状态
    private static final int RELEASE_REFRESH = 1; // 松开刷新
    private static final int REFRESHING = 2; // 正在刷新中
    public static int currentState = DOWN_PULL_REFRESH; // 头布局的状态: 默认为下拉刷新状态

    private Animation upAnimation; // 向上旋转的动画
    private Animation downAnimation; // 向下旋转的动画
    private View headView;
    private ImageView ivArrow;
    private ProgressBar progressBar;
    private TextView state;
    private TextView lastUpdateTime;
    private int headerViewHeight;

    private boolean isScrollToBottom;
    private View footerView;
    private int footerViewHeight;
    private boolean isLoadingMore = false;

    private Context context;

    public RefreshAndLoadListView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.context = context;
        initHeadView();
        initFooterView();
        this.setOnScrollListener(this);
    }

    private void initHeadView() {
        headView = LayoutInflater.from(context).inflate(R.layout.head, null);
        ivArrow = (ImageView) headView.findViewById(R.id.arrow);
        progressBar = (ProgressBar) headView.findViewById(R.id.progress_refresh);
        state = (TextView) headView.findViewById(R.id.state);
        lastUpdateTime = (TextView) headView.findViewById(R.id.lastupdate_time);
        lastUpdateTime.setText(getLastUpdateTime());
        headView.measure(0, 0);
        headerViewHeight = headView.getMeasuredHeight();
        Log.d(TAG, "initHeadView headerViewHeight " + headerViewHeight);
        headView.setPadding(0, -headerViewHeight, 0, 0);
        addHeaderView(headView);
        initAnimation();
    }

    private String getLastUpdateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(System.currentTimeMillis());
    }

    private void initAnimation() {
        upAnimation = new RotateAnimation(0f, -180f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        upAnimation.setDuration(500);
        upAnimation.setFillAfter(true); // 动画结束后, 停留在结束的位置上

        downAnimation = new RotateAnimation(-180f, -360f, Animation.RELATIVE_TO_SELF, 0.5f,
                Animation.RELATIVE_TO_SELF, 0.5f);
        downAnimation.setDuration(500);
        downAnimation.setFillAfter(true);
    }

    private void initFooterView() {
        footerView = LayoutInflater.from(context).inflate(R.layout.footer, null);
        footerView.measure(0, 0);
        footerViewHeight = footerView.getMeasuredHeight();
        footerView.setPadding(0, -footerViewHeight, 0, 0);
        addFooterView(footerView);
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int scrollState) {
        if (scrollState == SCROLL_STATE_IDLE || scrollState == SCROLL_STATE_FLING) {
            // 到了底部
            if (isScrollToBottom && !isLoadingMore) {
                isLoadingMore = true;
                Log.i(TAG, "加载更多数据");
                footerView.setPadding(0, 0, 0, 0);
                this.setSelection(this.getCount());

                if (onRefershListener != null) {
                    onRefershListener.onLoadingMore();
                }
            }
        }
    }

    @Override
    public void onScroll(AbsListView absListView, int _firstVisibleItem, int _visibleItemCount, int _totalItemCount) {
        firstVisiblePosition = _firstVisibleItem;
        if (getLastVisiblePosition() == (_totalItemCount - 1)) {
            isScrollToBottom = true;
        } else {
            isScrollToBottom = false;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN:
                dowmY = (int) ev.getY();
                Log.d(TAG, "onTouchEvent dowmY " + dowmY);
                break;
            case MotionEvent.ACTION_MOVE:
                int moveY = (int) ev.getY(); //移动后的坐标
                int space = (moveY - dowmY) / 2; //移动的距离
                Log.d(TAG, "onTouchEvent moveY " + moveY + " , space " + space);
                int paddingTop = -headerViewHeight + space;
                if (firstVisiblePosition == 0 && paddingTop > -headerViewHeight) {
                    // 完全显示
                    if (paddingTop > 0 && currentState == DOWN_PULL_REFRESH) {
                        Log.i(TAG, "松开刷新");
                        currentState = RELEASE_REFRESH;
                        refreshHeaderView();
                    }
                    // 没有显示完全
                    else if (paddingTop < 0 && currentState == RELEASE_REFRESH) {
                        Log.i(TAG, "下拉刷新");
                        currentState = DOWN_PULL_REFRESH;
                        refreshHeaderView();
                    }
                    headView.setPadding(0, paddingTop, 0, 0);
                    return true;
                }
                break;

            case MotionEvent.ACTION_UP:
                if (currentState == RELEASE_REFRESH) {
                    Log.i(TAG, "刷新数据.");
                    // 把头布局设置为完全显示状态
                    headView.setPadding(0, 0, 0, 0);
                    // 进入到正在刷新中状态
                    currentState = REFRESHING;
                    refreshHeaderView();

                    if (onRefershListener != null) {
                        onRefershListener.onDownPullRefresh();
                    }
                } else if (currentState == DOWN_PULL_REFRESH) {
                    // 隐藏头布局
                    headView.setPadding(0, -headerViewHeight, 0, 0);
                }
                break;
            default:
                break;
        }

        return super.onTouchEvent(ev);
    }

    private void refreshHeaderView() {
        switch (currentState) {
            case DOWN_PULL_REFRESH :
                state.setText("下拉刷新");
                ivArrow.startAnimation(downAnimation); // 执行向下旋转
                break;
            case RELEASE_REFRESH : // 松开刷新状态
                state.setText("松开刷新");
                ivArrow.startAnimation(upAnimation); // 执行向上旋转
                break;
            case REFRESHING : // 正在刷新中状态
                ivArrow.clearAnimation();
                ivArrow.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                state.setText("正在刷新中...");
                break;
            default :
                break;
        }
    }

    public void hideHeaderView() {
        headView.setPadding(0, -headerViewHeight, 0, 0);
        ivArrow.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        state.setText("下拉刷新");
        lastUpdateTime.setText("最后刷新时间: " + getLastUpdateTime());
        currentState = DOWN_PULL_REFRESH;
    }

    public void hideFooterView() {
        footerView.setPadding(0, -footerViewHeight, 0, 0);
        isLoadingMore = false;
    }

    public void setOnRefreshListener(OnRefreshListener listener) {
        onRefershListener = listener;
    }

    public interface OnRefreshListener {
        void onDownPullRefresh();
        void onLoadingMore();
    }
}
